package com.example.aleksey.hello1;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;


public class RestartGame extends Activity {


    private ImageButton mButm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.restartgame);


        mButm = (ImageButton) findViewById(R.id.imageButton7);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    // метод для перехода в главную активти из текущей активити
    public void Send(View view) {
        switch (view.getId()) {
            case R.id.imageButton7:

                Intent intent = new Intent(RestartGame.this, ActivMenu.class);
                startActivity(intent);

        }}



    // метод для перехода в рестарт уровня из текущей активити
    public void Sends(View view) {
        switch (view.getId()) {

         case R.id.imageButton9:

         Intent intents = new Intent(RestartGame.this, GameLvl2.class);
         startActivity(intents);

        }
    }



    public void Sends1(View view) {
        switch (view.getId()) {

            case R.id.imageButton10:

                Intent intents = new Intent(RestartGame.this, GameTest.class);
                startActivity(intents);

        }
    }



    @Override
    public void onBackPressed()
    {
        //ничего не делаем
    }
}
