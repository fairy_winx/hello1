package com.example.aleksey.hello1;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

/**
 * Created by Aleksey on 25.04.2018.
 */

public class ActivMenu extends AppCompatActivity implements View.OnClickListener {


    //основные поля класса главной активти
    private ImageButton mBut;
    private ImageButton stopmMyz;
    private TextView timerText;

    private static final String TAG = "MyService";
    MediaPlayer player;
    boolean flags = true;
    boolean homeFlags = true;
    boolean nextFlags = false;


    // main
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        // обращение к элементам разметки
        mBut = (ImageButton) findViewById(R.id.imageButton6);
        stopmMyz = (ImageButton) findViewById(R.id.imageButton8);

        // константа на книжную ориентацию экрана в активити
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        // Убрать ActionBar (заголовок)
        getSupportActionBar().hide();
        // старт фоновой музыки
        startService(new Intent(this, MyService.class));
    }


    //Обрабатываем нажатие кнопки "Передать"
    public void Send(View view) {
        switch (view.getId()) {
            case R.id.imageButton6:

                /**

                 //Создаем переход:
                 Intent intent = new Intent(MainActivity.this, GameTest.class);

                 //Запускаем переход:
                 startActivity(intent);
                 **/


                //Создаем переход:
                Intent intent = new Intent(ActivMenu.this, TransitionLvl2.class);

                //Запускаем переход:
                startActivity(intent);


        }}


    // метод на включение и выключение фоновой музыки в гланой активити
    public void Sends(View view){
        if (flags == true) {
            stopService(new Intent(this, MyService.class));
            stopmMyz.setImageResource(R.drawable.music_stop);
            flags = false;

        } else if (flags == false)  {
            startService(new Intent(this, MyService.class));
            stopmMyz.setImageResource(R.drawable.music_start);
            flags = true;

        }
    }




    // метод обработки нажатия кнопок
    public void onClick(View v) {
        //...
    }




    // метод не возврата в активти
    @Override
    public void onBackPressed()
    {
        //ничего не делаем
    }

}







