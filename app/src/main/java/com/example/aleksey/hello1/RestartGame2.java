package com.example.aleksey.hello1;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

/**
 * Created by Aleksey on 26.04.2018.
 */

public class RestartGame2 extends Activity {

    private ImageButton mButm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.restart_game2);


        mButm = (ImageButton) findViewById(R.id.imageButton7);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    // метод для перехода в главную активти из текущей активити
    public void Send(View view) {
        switch (view.getId()) {
            case R.id.imageButton7:

                Intent intent = new Intent(RestartGame2.this, ActivMenu.class);
                startActivity(intent);

        }}



    // метод для перехода в рестарт уровня из текущей активити
    public void Sends(View view) {
        switch (view.getId()) {

            case R.id.imageButton9:

                Intent intents = new Intent(RestartGame2.this, GameLvl2.class);
                startActivity(intents);

        }
    }





    @Override
    public void onBackPressed()
    {
        //ничего не делаем
    }
}
