package com.example.aleksey.hello1;

import android.app.Service;
import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.IBinder;
import android.util.Log;
import android.view.View;
import android.widget.Toast;


// класса работы с аудипотоками
public class MyService extends Service {
    private static final String TAG = "MyService";
    MediaPlayer player;


    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    // loop
    @Override
    public void onCreate() {
      //  Toast.makeText(this,"" , Toast.LENGTH_LONG).show();

        player = MediaPlayer.create(this, R.raw.background_music);
        player.setLooping(true); // зацикливаем
    }

    // остановка метода
    @Override
    public void onDestroy() {
      //  Toast.makeText(this, "", Toast.LENGTH_LONG).show();
        player.stop();
    }

    // старт метода
    @Override
    public void onStart(Intent intent, int startid) {
       // Toast.makeText(this, "", Toast.LENGTH_LONG).show();
        player.start();
    }


}