package com.example.aleksey.hello1;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;



import java.util.Timer;

public class GameTest extends Activity {
    //Объявляем поля, которые будем использовать:
    private TextView mT1;
    private TextView mT2;
    private Button mBack;

    //private TextView mHelloTextView;
    private ImageButton mBut;
    private ImageButton getmBut1;
    private ImageButton getmBut2;
    private ImageButton getmBut3;
    private ImageButton getmBut4;
    private int getmBut5;

    boolean flag = true;
    boolean inGame = true;

    private TextView numberSetGrubov;
    private int mCoutGrub = 0;

    private TextView timerText;
    private int timeSec = 60;
    Timer timer;


    private MainActivity aaTim;
    Timer aTimer = new Timer();
    private ImageButton getmB;



    private TextView mTimer; //1 1 1
    private int mmTimer;
    public   double millis = 20;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.gametest);

        mTimer = (TextView) findViewById(R.id.tvt);//1 1 1




        //Создаем таймер обратного отсчета на 20 секунд с шагом отсчета
        //в 1 секунду (задаем значения в миллисекундах):
        new CountDownTimer(20000, 1000) {

            //Здесь обновляем текст счетчика обратного отсчета с каждой секундой
            public void onTick(long millisUntilFinished) {

                mTimer.setText("Осталось секунд: "
                        + millisUntilFinished / 1000);
                

                }


            //Задаем действия после завершения отсчета (высвечиваем надпись):
            public void onFinish() {

                    mTimer.setText(" Для продолжения кликните на грибок.");
                    flag = false;

            }
        }
                .start();





    // обращение к элементам разметки
        getmBut4 = (ImageButton) findViewById(R.id.imageButton6);
        mBut = (ImageButton) findViewById(R.id.imageButton);
        getmBut1 = (ImageButton) findViewById(R.id.imageButton2);
        getmBut2 = (ImageButton) findViewById(R.id.imageButton3);
        getmBut3 = (ImageButton) findViewById(R.id.imageButton4);
        getmBut4 = (ImageButton) findViewById(R.id.imageButton5);


        numberSetGrubov = (TextView) findViewById(R.id.textView2);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        getmB = (ImageButton) findViewById(R.id.imageButton6);

    }


    // метод на сбор грибочков
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.imageButton:

                mBut.setVisibility(View.GONE);
                numberSetGrubov.setText("Грибов собрано: " + ++mCoutGrub + "/5");

                break;

            case R.id.imageButton2:
                getmBut1.setVisibility(View.GONE);
                numberSetGrubov.setText("Грибов собрано: " + ++mCoutGrub + "/5");
                break;

            case R.id.imageButton3:
                getmBut2.setVisibility(View.GONE);
                numberSetGrubov.setText("Грибов собрано: " + ++mCoutGrub + "/5");
                break;

            case R.id.imageButton4:
                getmBut3.setVisibility(View.GONE);
                numberSetGrubov.setText("Грибов собрано: " + ++mCoutGrub + "/5");
                break;

            case R.id.imageButton5:
                getmBut4.setVisibility(View.GONE);
                numberSetGrubov.setText("Грибов собрано: " + ++mCoutGrub + "/5");
                break;

        }

        // проверка на правильный сбор грибочков и отсчёт по таймеру

        if (mCoutGrub == 5 && flag != false) {
            numberSetGrubov.setText("Все грибы собраны!");

            Intent intent = new Intent(GameTest.this, RestartGame.class);
            startActivity(intent);

        } else if(flag == false){
            Intent intents=new Intent(GameTest.this,ResetGameOnTime.class);
            startActivity(intents);
        }

    }

    // метод не возврата в активти
    @Override
    public void onBackPressed()
    {
        //ничего не делаем
    }


}