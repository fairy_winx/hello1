package com.example.aleksey.hello1;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

/**
 * Created by Aleksey on 26.04.2018.
 */

public class ResetGameOnTime2 extends Activity {



    private ImageButton setB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.reset_game_on_time2);


        setB = (ImageButton) findViewById(R.id.setB);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    // метод для перехода по опциям для окончания таймера
    // переход в главную активити
    public void Send(View view) {
        switch (view.getId()) {

            case R.id.setB:

                Intent intent = new Intent(ResetGameOnTime2.this, ActivMenu.class);
                startActivity(intent);

        }}

    // переход на второй уровень
    public void Sends(View view) {
        switch (view.getId()) {
            case R.id.setBut:

                Intent intents = new Intent(ResetGameOnTime2.this, GameLvl2.class);
                startActivity(intents);

        }
    }



    @Override
    public void onBackPressed()
    {
        //ничего не делаем
    }
}





