package com.example.aleksey.hello1;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;



public class ResetGameOnTime extends Activity {

    private ImageButton setB;

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.resetgameontime);


            setB = (ImageButton) findViewById(R.id.setB);
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }

        // метод для перехода по опциям для окончания таймера
        // переход в главную активити
        public void Send(View view) {
            switch (view.getId()) {

                case R.id.setB:

                    Intent intent = new Intent(ResetGameOnTime.this, MainActivity.class);
                    startActivity(intent);

            }}

        // переход на второй уровень
        public void Sends(View view) {
            switch (view.getId()) {
                case R.id.setBut:

                    Intent intents = new Intent(ResetGameOnTime.this, GameLvl2.class);
                    startActivity(intents);

            }
        }



        @Override
        public void onBackPressed()
        {
            //ничего не делаем
        }
    }





