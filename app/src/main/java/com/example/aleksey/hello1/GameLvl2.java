package com.example.aleksey.hello1;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.Timer;

/**
 * Created by Aleksey on 20.04.2018.
 */

public class GameLvl2 extends Activity {


    private ImageButton getmBut3;
    private ImageButton getmBut4;
    private ImageButton getmBut5;
    private ImageButton getmBut6;
    private ImageButton getmBut7;
    private ImageButton getmBut8;
    private ImageButton getmBut9;
    private ImageButton getmBut10;
    private ImageButton getmBut11;
    private ImageButton getmBut12;
    private ImageButton getmBut13;
    private ImageButton getmBut14;
    private ImageButton getmBut15;
    private ImageButton getmBut16;
    private ImageButton getmBut17;


    TextView mTimer1;
    boolean flag = true;

    private TextView numberSetGrubov;
    private int mCoutGrub = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.game_lvl_2);

        mTimer1 = (TextView) findViewById(R.id.tvt1);//1 1 1



        //Создаем таймер обратного отсчета на 20 секунд с шагом отсчета
        //в 1 секунду (задаем значения в миллисекундах):
        new CountDownTimer(40000, 1000) {

            //Здесь обновляем текст счетчика обратного отсчета с каждой секундой
            public void onTick(long millisUntilFinished) {

                mTimer1.setText("Осталось секунд: "
                        + millisUntilFinished / 1000);


            }


            //Задаем действия после завершения отсчета (высвечиваем надпись):
            public void onFinish() {

                mTimer1.setText(" Для продолжения кликните на грибок.");
                flag = false;

            }
        }
                .start();




        // обращение к элементам разметки
        getmBut3 = (ImageButton) findViewById(R.id.imageButton3);
        getmBut4 = (ImageButton) findViewById(R.id.imageButton4);
        getmBut5 = (ImageButton) findViewById(R.id.imageButton5);
        getmBut6 = (ImageButton) findViewById(R.id.imageButton6);
        getmBut7 = (ImageButton) findViewById(R.id.imageButton7);
        getmBut8 = (ImageButton) findViewById(R.id.imageButton8);
        getmBut9 = (ImageButton) findViewById(R.id.imageButton9);
        getmBut10 = (ImageButton) findViewById(R.id.imageButton10);
        getmBut11 = (ImageButton) findViewById(R.id.imageButton11);
        getmBut12 = (ImageButton) findViewById(R.id.imageButton12);
        getmBut13 = (ImageButton) findViewById(R.id.imageButton13);
        getmBut14 = (ImageButton) findViewById(R.id.imageButton14);
        getmBut15 = (ImageButton) findViewById(R.id.imageButton15);
        getmBut16 = (ImageButton) findViewById(R.id.imageButton16);
        getmBut17 = (ImageButton) findViewById(R.id.imageButton17);




        numberSetGrubov = (TextView) findViewById(R.id.textView2);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

      //  getmB = (ImageButton) findViewById(R.id.imageButton6);

    }


    // метод на сбор грибочков
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.imageButton3:
                getmBut3.setVisibility(View.GONE);
                numberSetGrubov.setText("Грибов собрано: " + ++mCoutGrub + "/15");
                break;

            case R.id.imageButton4:
                getmBut4.setVisibility(View.GONE);
                numberSetGrubov.setText("Грибов собрано: " + ++mCoutGrub + "/15");
                break;


            case R.id.imageButton5:
                getmBut5.setVisibility(View.GONE);
                numberSetGrubov.setText("Грибов собрано: " + ++mCoutGrub + "/15");
                break;


            case R.id.imageButton6:
                getmBut6.setVisibility(View.GONE);
                numberSetGrubov.setText("Грибов собрано: " + ++mCoutGrub + "/15");
                break;


            case R.id.imageButton7:
                getmBut7.setVisibility(View.GONE);
                numberSetGrubov.setText("Грибов собрано: " + ++mCoutGrub + "/15");
                break;

            case R.id.imageButton8:
                getmBut8.setVisibility(View.GONE);
                numberSetGrubov.setText("Грибов собрано: " + ++mCoutGrub + "/15");
                break;

            case R.id.imageButton9:
                getmBut9.setVisibility(View.GONE);
                numberSetGrubov.setText("Грибов собрано: " + ++mCoutGrub + "/15");
                break;

            case R.id.imageButton10:
                getmBut10.setVisibility(View.GONE);
                numberSetGrubov.setText("Грибов собрано: " + ++mCoutGrub + "/15");
                break;

            case R.id.imageButton11:
                getmBut11.setVisibility(View.GONE);
                numberSetGrubov.setText("Грибов собрано: " + ++mCoutGrub + "/15");
                break;

            case R.id.imageButton12:
                getmBut12.setVisibility(View.GONE);
                numberSetGrubov.setText("Грибов собрано: " + ++mCoutGrub + "/15");
                break;

            case R.id.imageButton13:
                getmBut13.setVisibility(View.GONE);
                numberSetGrubov.setText("Грибов собрано: " + ++mCoutGrub + "/15");
                break;

            case R.id.imageButton14:
                getmBut14.setVisibility(View.GONE);
                numberSetGrubov.setText("Грибов собрано: " + ++mCoutGrub + "/15");
                break;

            case R.id.imageButton15:
                getmBut15.setVisibility(View.GONE);
                numberSetGrubov.setText("Грибов собрано: " + ++mCoutGrub + "/15");
                break;

            case R.id.imageButton16:
                getmBut16.setVisibility(View.GONE);
                numberSetGrubov.setText("Грибов собрано: " + ++mCoutGrub + "/15");
                break;

            case R.id.imageButton17:
                getmBut17.setVisibility(View.GONE);
                numberSetGrubov.setText("Грибов собрано: " + ++mCoutGrub + "/15");
                break;



        }

        // проверка на правильный сбор грибочков и отсчёт по таймеру

        if (mCoutGrub == 15 && flag != false) {
            numberSetGrubov.setText("Все грибы собраны!");

            Intent intent = new Intent(GameLvl2.this, RestartGame2.class);
            startActivity(intent);

        } else if(flag == false){
            Intent intents=new Intent(GameLvl2.this,ResetGameOnTime.class);
            startActivity(intents);
        }

    }

    // метод не возврата в активти
    @Override
    public void onBackPressed()
    {
        //ничего не делаем
    }


}
