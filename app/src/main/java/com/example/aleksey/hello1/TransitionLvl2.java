package com.example.aleksey.hello1;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;

/**
 * Created by Aleksey on 25.04.2018.
 */

public class TransitionLvl2 extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.transition_lvl2);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

    }

    //Обрабатываем нажатие кнопки "Передать"
    public void Send(View view) {
        switch (view.getId()) {
            case R.id.lvl1:

                //Создаем переход:
                Intent intent = new Intent(TransitionLvl2.this, GameTest.class);
                //Запускаем переход:
                startActivity(intent);
        }}


     public void Sends(View view) {
     switch (view.getId()) {
     case R.id.lvl2:


     Intent intent = new Intent(TransitionLvl2.this, GameLvl2.class);
     startActivity(intent);
     }}



    // метод не возврата в активти
    @Override
    public void onBackPressed()
    {
        //ничего не делаем
    }


}


